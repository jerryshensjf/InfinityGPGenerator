#  无垠式Java通用代码生成器

### 软件介绍

本软件是无垠式java通用代码生成器0.8.5（Code Name:Trinity 崔妮蒂）的全部源码。
本软件是Java Web通用代码生成器平台，是通用的加快开发速度的工具。支持s2sh,s2shc和simplejee三个技术栈。
开发环境是 Java 7开发工具 Eclipse JEE版全部源码在GPL v2版条款下开源，GPL v2的文本记录在gpl2.txt中本软件包含三个技术栈：Clocksimplejee(jsp,simplejee)栈，s2sh栈和s2shc栈。

### 研发历史

此分支曾经是动词算子式代码生成器阵列的唯一分支,现在处于维护状态。在2014-2016间，是唯一的动词算子式代码生成器的实现。
无垠式/和平之翼代码生成器阵列的四大旗舰分支为：无垠式代码生成器JEEEU版，和平之翼代码生成器SMEU版，和平之翼代码生成器SHCEU版和第三代动词算子式代码生成器光SBMEU版。

此代码生成器已升级至0.8.5，将SGS编辑器升级成语法加亮的SGS代码编辑器，并修正了样例程序中的错误，其后，此代码生成器将不定期冻结。
其二进制发布版的war包可在本站附件下载。[https://gitee.com/jerryshensjf/InfinityGPGenerator/attach_files](https://gitee.com/jerryshensjf/InfinityGPGenerator/attach_files)

此代码生成器是无垠式/和平之翼动词算子式代码生成器阵列的最早成员，拥有三个技术栈，其中的S2SH和S2SHC技术栈为此代码生成器独有，虽说功能不是很先进，但是升级代码编辑器以后，还是一个很实用的代码生成器。

### 第一个完整版本
无垠式代码生成器的第一个完整版本是无垠式代码生成器0.6.5。此版本是学习本系列代码生成器的捷径，有重要的历史意义。完成于2014.8.3。对动词算子式代码生成器阵列而言，这个版本就相当于Linux 0.0.1

项目地址：[https://gitee.com/jerryshensjf/InfinityGPGenerator_0_6_5](https://gitee.com/jerryshensjf/InfinityGPGenerator_0_6_5)

### 后继版本
此软件的后继版本是和平之翼代码生成器SMEU版，和平之翼代码生成器SHCEU版,无垠式代码生成器JEEEU版和第三代动词算子式代码生成器光SBMEU版。而软件本身InfinityGPGenerator将处于维护状态。

和平之翼代码生成器SMEU版项目地址：

[https://gitee.com/jerryshensjf/PeaceWingSMEU](https://gitee.com/jerryshensjf/PeaceWingSMEU)

和平之翼代码生成器SHCEU版项目地址：

[https://gitee.com/jerryshensjf/PeaceWingSHCEU](https://gitee.com/jerryshensjf/PeaceWingSHCEU)

无垠式代码生成器JEEEU版项目地址：

[https://gitee.com/jerryshensjf/InfinityJEEEU](https://gitee.com/jerryshensjf/InfinityJEEEU)

第三代动词算子式代码生成器光SBMEU版：

[https://gitee.com/jerryshensjf/LightSBMEU](https://gitee.com/jerryshensjf/LightSBMEU)

### 项目图片

软件界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/1027/132756_941a291d_1203742.png "SGS_0_8_5.png")

崔妮蒂

![输入图片说明](https://gitee.com/uploads/images/2018/0206/142003_9fd7a7af_1203742.jpeg "Trinity.jpg")

合成图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1028/165210_9d9a77d7_1203742.jpeg "trinity_ui.jpg")

### 动词算子的力量

向Lisp和Lambda算子致敬

愿动词算子的力量与你同在

![输入图片说明](https://images.gitee.com/uploads/images/2019/0109/211947_432567db_1203742.png "magic_v.png")

### 无垠式代码生成器图标，核火箭：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1105/161255_92a7451c_1203742.jpeg "infinity.jpg")